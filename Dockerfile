FROM python:3.9.0

ARG ImgHost=pypi.tuna.tsinghua.edu.cn

RUN mkdir -p /opt/deployments

COPY ./requirements.txt /opt/deployments

RUN pip install --trusted-host=${ImgHost} --no-cache -r /opt/deployments/requirements.txt \
    -i https://${ImgHost}/simple

COPY ./code /opt/deployments

WORKDIR /opt/deployments

CMD ["uvicorn", "main:app", "--host=0.0.0.0", "--port=8000", "--workers=2"]
