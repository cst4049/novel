from typing import List
from pydantic import BaseSettings, Field


class AppSettings(BaseSettings):
    host: str = Field('0.0.0.0', title='主机')
    port: int = Field(9000, title='端口')
    blueprints: List[str] = Field(['heartbeat', 'novel', 'botn', 'content'], title='蓝图')


class MongoSettings(BaseSettings):
    db_uri: str = Field('mongodb://localhost:27017/test')
    db_novel: str = Field('novels')
    col_novel: str = Field('novel')
    col_botn: str = Field('catalog')
    col_content: str = Field('chapter')


class LogSettings(BaseSettings):
    log_name: str = Field('novel', title='log名')
    log_level: str = Field('INFO', title='log等奖')
    log_format: str = Field('%(asctime)s - %(name)s - %(levelname)s - %(message)s', title='log样式')


app_settings = AppSettings()
mongo_settings = MongoSettings()
log_settings = LogSettings()
