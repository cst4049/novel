from enum import Enum
from functools import wraps
from app.common import logger


class ResponseCode(Enum):
    """响应码"""
    Success = 0
    Faild = -1
    Unknow = -2


def make_response(func):
    @wraps(func)
    def decorate(*args, **kwargs):
        try:
            code, msg, data = func(*args, **kwargs)
            resp = {'code': code, 'msg': msg, 'data': data}
        except Exception as e:
            resp = {'code': ResponseCode.Faild.value, 'msg': '异常错误', 'data': {}}
        finally:
            logger.info(f'返回值: {resp["code"]}')
            return resp
    return decorate


if __name__ == '__main__':
    logger.info('aaaa')
