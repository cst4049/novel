import json
from fastapi import APIRouter
from app.utils import make_response, ResponseCode
from app.common import mongo_client, JSONEncoder
from bson import ObjectId
from app.schemas.content import QBotnContent
from settings import mongo_settings


bp = APIRouter()


@make_response
def content(body: QBotnContent):
    """章节目录"""
    req_json = body.dict()
    _id = ObjectId(req_json['id'])
    query = {'_id': _id}

    result = mongo_client.execute(
        mongo_client.client[mongo_settings.db_novel][mongo_settings.col_content].find_one, query
    )
    if result:
        return ResponseCode.Success.value, '', json.loads(JSONEncoder().encode(result))
    return ResponseCode.Success.value, '', {}


bp.add_api_route(path='/content', name='content', endpoint=content, methods=['POST'], tags=['章节内容'])
