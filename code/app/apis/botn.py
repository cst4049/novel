import json
from fastapi import APIRouter
from app.utils import make_response, ResponseCode
from app.common import mongo_client, JSONEncoder
from bson import ObjectId
from app.schemas.botn import QBookBotn
from settings import mongo_settings


bp = APIRouter()


@make_response
def botn(body: QBookBotn):
    """章节目录"""
    req_json = body.dict()
    _id = ObjectId(req_json['id'])
    query = {'_id': _id}

    result = mongo_client.execute(
        mongo_client.client[mongo_settings.db_novel][mongo_settings.col_botn].find_one, query
    )
    if result:
        return ResponseCode.Success.value, '', json.loads(JSONEncoder().encode(result))
    return ResponseCode.Success.value, '', {}


bp.add_api_route(path='/botn', name='botn', endpoint=botn, methods=['POST'], tags=['章节目录'])
