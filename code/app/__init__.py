from importlib import import_module
from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

from settings import app_settings, mongo_settings
from app.common import logger, mongo_client


def register_blueprints(app: FastAPI, blueprints: list[str]):
    """蓝图注册"""
    for bp in blueprints:
        mod = import_module(f'app.apis.{bp}')
        app.include_router(mod.bp)


def register_common():
    """公共组件注册"""
    mongo_client.init_config(mongo_settings.db_uri, mongo_settings.db_novel)


def create_app():
    """app 实例化"""
    app = FastAPI()
    register_blueprints(app, app_settings.blueprints)
    register_common()

    app.mount('/static', StaticFiles(directory='static'), name='static')
    return app
