import threading
import logging
from settings import log_settings


class MyLogger(logging.Logger):

    _instance_lock = threading.Lock()

    def __init__(self, name, level, log_format):
        super().__init__(name, level)
        if self.handlers:
            self.info('已经示例化')
            return
        self.setLevel(level=level)

        console = logging.StreamHandler()
        console.setLevel(level)
        console.setFormatter(logging.Formatter(log_format))
        self.addHandler(console)

    def __new__(cls, *args, **kwargs):
        if not hasattr(MyLogger, '_instance'):
            with MyLogger._instance_lock:
                if not hasattr(MyLogger, '_instance'):
                    MyLogger._instance = object.__new__(cls)
        return MyLogger._instance


logger = MyLogger(log_settings.log_name, log_settings.log_level, log_settings.log_format)
