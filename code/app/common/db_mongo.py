import json
from bson import ObjectId
from pymongo import MongoClient


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)


class MongoClientA:
    def __init__(self):
        self.client = None
        self.db = None

    def init_config(self, uri, db):
        self.client = MongoClient(uri)
        self.db = db

    @staticmethod
    def execute(func, *args, **kwargs):
        return func(*args, **kwargs)


mongo_client = MongoClientA()
