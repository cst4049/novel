from app.schemas.common import BaseModel, Field, List


class Content(BaseModel):
    """章节信息"""
    id: str = Field(..., title='章节id', alias='_id')
    name: str = Field(..., title='章节名')
    content: str = Field(..., title='章节内容')
    href: str = Field(..., title='原文链接')
    img: List[str] = Field(..., title='图片链接')
    dimg: List[str] = Field(..., title='图片链接')


class QBotnContent(BaseModel):
    id: str = Field(..., title='章节id', alias='_id')
