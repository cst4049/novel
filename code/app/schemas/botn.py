from app.schemas.common import _Page, BaseModel, Field, List


class Botn(BaseModel):
    """章节信息"""
    id: str = Field(..., title='章节id', alias='_id')
    name: str = Field(..., title='章节名')
    type: str = Field(..., title='章节类型')
    dad: str = Field(..., title='父节点')
    son: List[str] = Field(..., title='子节点')


class QBookBotn(BaseModel):
    id: str = Field(..., title='书籍id', alias='_id')
