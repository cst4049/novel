from app.schemas.common import _Page, BaseModel, Field, List


class Novel(BaseModel):
    """书籍信息"""
    name: str = Field(..., title='书名')
    author: str = Field(..., title='作者')
    num: str = Field(..., title='书籍编号（来源网）')
    blurb: str = Field(..., title='简介')
    updated_at: str = Field(..., title='更新时间')
    similar: List[str] = Field(..., title='相似书籍')
    state: List[str] = Field(..., title='完本状态')
    words: List[str] = Field(..., title='字数')


class _QKeyWord(BaseModel):
    name: str = Field(None, title='搜索名称')
    author: str = Field(None, title='搜索作者')


class Query(_Page):
    q: _QKeyWord = Field(None, title='查询条件')
