from typing import List
from pydantic import BaseModel, Field


class _Page(BaseModel):
    """翻页功能"""
    page: int = Field(1, title='页码')
    count: int = Field(10, title='每页条数', max=100)
