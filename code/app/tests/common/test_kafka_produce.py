import json
from settings import kfk_prd_settings
from app.common.mq_kafka import kfk_producer


kfk_producer.init_config(**kfk_prd_settings.dict(by_alias=True))


def test_send_msg():
    msg = {'a': '1', 'b': '3'}
    msg = json.dumps(msg)
    kfk_producer.send_msg(msg, topic=kfk_prd_settings.topic)


test_send_msg()
