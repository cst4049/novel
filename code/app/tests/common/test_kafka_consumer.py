from settings import kfk_csm_settings
from app.common.mq_kafka import kfk_consumer

print(kfk_csm_settings.dict(by_alias=True))
kfk_consumer.init_config(**kfk_csm_settings.dict(by_alias=True))


def test_get_msg():
    for msg in kfk_consumer.get_msg(1):
        print(msg)


test_get_msg()
